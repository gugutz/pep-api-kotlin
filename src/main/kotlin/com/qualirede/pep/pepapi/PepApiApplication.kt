package com.qualirede.pep.pepapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PepApiApplication

fun main(args: Array<String>) {
	runApplication<PepApiApplication>(*args)
}
