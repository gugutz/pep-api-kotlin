wget -o /tmp/spring-boot.tar.gz https://repo.spring.io/release/org/springframework/boot/spring-boot-cli/2.2.4.RELEASE/spring-boot-cli-2.2.4.RELEASE-bin.tar.gz

tar -xvzf spring-boot.tar.gz --directory /tmp/spring-boot

mv /tmp/spring-boot /opt

export PATH=$PATH:/opt/spring-boot/bin >> ~/.bashrc
if [ -f "~/.zshrc" ]; then
  export PATH=$PATH:/opt/spring-boot/bin >> ~/.bashrc
fi

DIR=~/.asdf
if [ ! -d "$DIR" ]; then
    echo "## installing asdf"
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.6
else
    echo "asdf was already found in this system on $DIR"
fi

asdf plugin-add gradle
asdf install gradle 6.1.1
asdf global gradle 6.1.1